define(["jquery", "urijs/URI", "app/dao-0.1", "app/popups-0.1"], function ($, URI, dao, popups) {
	var clientPageElement = $('.client-page');
	var orderListElement = $('.order-list');
	var $statusDroplist = $('.status-select');
	var currentURI = new URI();

	if (currentURI.hasQuery('statusId')) {
		$statusDroplist.val(currentURI.query(true).statusId);
	}

	$statusDroplist.change(function() {
		var selectedStatusId = $(this).val();
		var currentURI = new URI();
		var newURI;
		if (selectedStatusId == 0) {
			newURI = currentURI.removeQuery("statusId");
		} else {
			newURI = currentURI.setQuery({statusId: selectedStatusId});
		}
		document.location = newURI;
	});

	orderListElement.on( 'click', '.order-list-item', function() {
		var orderData = {id: $(this).data().orderId};
		var orderElement = $(this);
		popups.orderHistory.show(orderData)
		.done( function (newStatus) {
			orderElement.find('.order-status').text(newStatus);
		});
	});

	clientPageElement.find('.add-order-button').click(function() {
		popups.orderAdd.show({id:$(this).data().clientId})
		.done(function(newOrder) {
			document.location = document.location.origin + document.location.pathname;
		});
	});
});
