package com.epam.javatraining2016.springmvcfrontend.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.UserSearchResult;
import com.epam.javatraining2016.springmvcfrontend.support.CurrentUser;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@Controller
@RequestMapping(value = "/login")
public class Login {

  @GetMapping
  public ModelAndView get() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("login");
    return mav;
  }

  @PostMapping
  public ModelAndView post(@RequestParam String username, @RequestParam String password) {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("redirect:/clients");
    return mav;
  }
}
