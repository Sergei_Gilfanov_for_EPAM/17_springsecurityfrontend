package com.epam.javatraining2016.springmvcfrontend.controller.ajax;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@RestController
@RequestMapping(value = "/backend/loginAvailable")
public class LoginAvailable implements SoapUser {

  @PostMapping
  public boolean get(@RequestParam String login, HttpServletResponse response) {
    boolean retval = soapEndPoint().loginAvailable(login);
    if (retval) {
      response.setStatus(HttpServletResponse.SC_OK);
    } else {
      response.setStatus(HttpServletResponse.SC_CONFLICT);
    }
    return retval;
  }
}
