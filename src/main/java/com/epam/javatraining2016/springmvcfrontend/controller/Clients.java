package com.epam.javatraining2016.springmvcfrontend.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Client;
import com.epam.javatraining2016.springmvcfrontend.support.CurrentUser;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@Controller
@RequestMapping(value = "/clients")
public class Clients implements SoapUser {
  //private static final Logger log = LoggerFactory.getLogger(Clients.class);

  @GetMapping
  public ModelAndView get(@AuthenticationPrincipal CurrentUser currentUser ) {
    ModelAndView mav = new ModelAndView();
    mav.addObject("currentUser", currentUser);
    List<Client> clients = soapEndPoint().getClients();
    mav.addObject("clients", clients);
    mav.setViewName("clients");
    return mav;
  }
}
